import { ModuleTree } from 'vuex';
import { RootState } from './types';

const modules: ModuleTree<RootState> = {

};

export default modules;
