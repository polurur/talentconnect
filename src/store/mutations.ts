import { MutationTree } from 'vuex';
import { RootState } from './types';

/* Setup Module Mutations Here */
const mutations: MutationTree<RootState> = {

};

export default mutations;
