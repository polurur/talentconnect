import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';

import { RootState } from './types';
import state from './state';
import modules from './modules';
import mutations from './mutations';
import getters from './getters';


Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  state,
  modules,
  mutations,
  getters,
};

export default new Vuex.Store<RootState>(store);
