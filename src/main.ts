import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import '@/scss/main.scss';

import { auth } from './common/auth';

import { installGlobalPlugins } from './setup';
installGlobalPlugins();

Vue.config.productionTip = false;

// Uncomment for oAuth post Sandbox setup

/* auth().then(() => {
  return new Vue({
    router,
    store,
    render: (h) => h(App),
  });
}).then((vm) => {
  vm.$mount('#app');
}); */

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
