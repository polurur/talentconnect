import { Proteus } from '@zeta/http-client';
import { AUTH_TOKEN_KEY } from '@zeta/authentication';
import { dotprop } from '@zeta/utils';
import authenticationStore from '@zeta/authentication/lib/storage';
import { NotificationProgrammatic } from 'buefy/dist/components/notification';

export const proteus = Proteus.create({
    baseURL: '', // use __APP__.BASE_URL and 'injectConfig' in vue.config.js file 
    resolveAuthToken() {
        return {
            Authorization: `Bearer ${authenticationStore.getItem(AUTH_TOKEN_KEY)}`,
        };
    },
});

// Interceptors
proteus.interceptors.request.use(
    (request) => {
        return request;
    },
    (error) => {
        return Promise.reject(error);
    },
);

proteus.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        const message = dotprop.get(
            error.response,
            'data.message',
            'Oops! Something Went Wrong',
        );

        NotificationProgrammatic.open({
            message,
            duration: 5000,
            type: 'is-danger',
            position: 'is-bottom-right',
            hasIcon: true,
        });

        return Promise.reject(error);
    },
);
