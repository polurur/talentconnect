export const SIDEBAR_CONFIG = [
  {
    title: '', // Section Title - if you don't have the title then leave it blank
    config: [
      {
        name: 'Page 1 Link', // Module name
        icon: 'mdi item-icon mdi-file-document', // Module icon
        path: '/home', // Absolute route path
      },
    ],
  },
];
