import { authenticate, OAuthAuthenticator, URLAuthenticator } from '@zeta/authentication';
import { getURLParameter } from '@zeta/utils';

export const auth = () => {
    return authenticate(
        getURLParameter('code') ?
            new OAuthAuthenticator({
                baseUrl: '', // use __APP__.OAUTH.BASE_URL from vue.config.js
                // secret: '', // Procure during Sandbox Setup
                loginParams: {
                    client_id: '', // Procure during Sandbox Setup
                    domainId: '', // Procure during Sandbox Setup
                    redirect_uri: window.location.origin,
                    scope: '', // 'scope' of the app. Procure during Sandbox Setup
                    response_type: 'code', 
                    state: '', // 'state' of app. Procure during Sandbox Setup
                },
                beforeLogin(): void {
                    // Code snippet to do something before logging in
                },
            }) : new URLAuthenticator(),
    );
};
