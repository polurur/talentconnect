import { TalentconnectServices } from './manager/index';
import { proteus } from '@/common/proteus';

export const TalentconnectManager = new TalentconnectServices(proteus);