declare const __APP__: any;
declare const __LOCAL__: boolean;
declare const __STAGE__: boolean;
declare const __PREPROD__: boolean;
declare const __PROD__: boolean;

declare module 'buefy/dist/components/notification';
declare module '@zeta/aph-components';
declare module '@/common/constants';