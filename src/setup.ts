import Vue, { PluginObject, PluginFunction } from 'vue';
import Buefy from 'buefy';

type PluginList = [PluginObject<any> | PluginFunction<any>, any];

const plugins: PluginList[] = [
    [Buefy, {}],
    // Add more plugins [<PluginName>, <PluginConfig>]
];

export const installGlobalPlugins = () => {
    plugins.forEach((item) => {
        const [plugin, options] = item;
        Vue.use(plugin, options);
    });
};

