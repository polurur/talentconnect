import { ActionTree } from "vuex";
import { HomeState } from './home.types';
import { RootState } from '@/store/types';

/* Setup Module Actions here */
const actions: ActionTree<HomeState, RootState> = {
    /*
        // Sample Action
        [SAMPLE_ACTION]({rootState, commit}) {
            // API call or Logic here
            commit(MUTATION_NAME, data);
        }
     */
};

export default actions;
