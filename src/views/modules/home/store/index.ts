import { Module } from 'vuex';
import { namespace } from 'vuex-class';

import { HomeState } from './home.types';
import { RootState } from '@/store/types';

import state from './home.state';
import actions from './home.actions';
import getters from './home.getters';
import mutations from './home.mutations';

const module: Module<HomeState, RootState> = {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};

export const HOME_STORE = 'home';

export const HomeModule = namespace(HOME_STORE);

export default module;
