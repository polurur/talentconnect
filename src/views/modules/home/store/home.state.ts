import { HomeState } from "./home.types";

/* Setup Module Initial State Here */
const state: HomeState = {
    /*
        // Sample State Attribute
        attr: "<Attribute Value>"
    */
};

export default state;
