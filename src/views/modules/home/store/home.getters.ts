import { GetterTree } from 'vuex';
import { HomeState } from './home.types';
import { RootState } from '@/store/types';

/* Setup Module Getter Functions here */
const getters: GetterTree<HomeState, RootState> = {
    /*
        // Sample Getter Function
        getModuleState(state): Type {
            return state.attr;
        }

     */
};

export default getters;
