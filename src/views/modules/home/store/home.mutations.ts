import { MutationTree } from 'vuex';
import { HomeState } from './home.types';

/* Setup Module Mutations HERE */
const mutations: MutationTree<HomeState> = {
    /*
        // Sample Mutation
        [MUTATION_NAME](state, payload) {
            state.attr = payload;
        }
     */
};

export default mutations;
