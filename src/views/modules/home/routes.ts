import { RouteConfig } from 'vue-router';

import Homepage from './pages/Homepage.vue';
import Statuspage from './pages/Statuspage.vue';

const routes: RouteConfig[] = [
    {
        path: '/home',
        component: Homepage,
    },
    {
        path: '/status',
        component: Statuspage,
    },

];

export default routes;