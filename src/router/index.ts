import Vue from 'vue';
import VueRouter from 'vue-router';
// import Home from '../views/Home.vue';

import HomePageRoute from '@/views/modules/home/routes';

Vue.use(VueRouter);

const routes = [
  {
    path:'/',
    redirect: '/home'
  },
  ...HomePageRoute,
];

const router = new VueRouter({
  routes,
});

export default router;
