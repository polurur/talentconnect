const {
  APP_ENV,
  isAppEnvEquals,
  injectConfig,
  resolveEnv
} = require('@zeta/environment');
const appConfig = require('./package.json');
const { PORT } = require('./config/dev.config');

module.exports = {
  lintOnSave: false,
  devServer: {
    port: PORT
  },

  publicPath: './',
  productionSourceMap: !isAppEnvEquals('PROD'),
  configureWebpack: {
    performance: {
      maxEntrypointSize: 512000,
      maxAssetSize: 512000
    },
    plugins: [
      injectConfig({
        ENV: APP_ENV,
        NAME: appConfig.name,
        VERSION: appConfig.version,
        OAUTH: {
          BASE_URL: resolveEnv({
            STAGE: 'https://oauth-stage.zetaapps.in',
            PREPROD: 'https://oauth-pp.zetaapps.in/',
            PROD: 'https://oauth.zetaapps.in/'
          })
        },
        DOMAIN: resolveEnv({
          STAGE: 'aphrodite_ops_center'
        })
      })
    ]
  }
};
