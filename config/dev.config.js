const { resolveEnv } = require("@zeta/environment");

module.exports = {
  PORT: resolveEnv({
    STAGE: 9000,
    PREPROD: 9000
  })
};
